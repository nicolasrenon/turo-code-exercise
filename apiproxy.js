const express = require('express')
const cors = require('cors')
const request = require('request-promise')

const app = express()

// Hotwire configuration
const hw = {
  uri: 'http://api.hotwire.com/v1/search/car',
  apikey: '7u2n59qgqcehc2d7ad7u5b6p',
}

// Google Places API configuration
const gp = {
  uri: 'https://maps.googleapis.com/maps/api/place/autocomplete/json',
  key: 'AIzaSyD4SErW8mc98vIesGawJr4uwweW5PGw1iA',
}

// Calls Hotwire with the proxy request's parameters and returns the results
const hotwire = ({ dest, startdate, enddate, pickuptime, dropofftime }) => {
  const { uri, apikey } = hw
  return request({
    uri,
    qs: {
      apikey,
      dest,
      startdate,
      enddate,
      pickuptime,
      dropofftime,
      format: 'json',
    },
    headers: { 'User-Agent': 'Request-Promise' },
    json: true,
  })
    // It's necessary to test Result and to throw an error if it's undefined
    // because in the query of the request all the parameters are set above,
    // with undefined as default value, even if they are missing in the original proxy request
    .then(({ MetaData: { CarMetaData: { CarTypes } }, Result }) => {
      if (Result) {
        return { types: CarTypes, cars: Result }
      }
      return Promise.reject('Something went wrong with the request!')
    })
    .catch(error => error)
}

// Calls Google Places API with the requested location and returns the results
const googleplaces = ({ input }) => {
  const { uri, key } = gp
  return request({
    uri,
    qs: {
      key,
      input,
      components: 'country:us',
    },
    headers: { 'User-Agent': 'Request-Promise' },
    json: true,
  })
    .then(({ predictions }) => predictions)
    .catch(error => error)
}

app.use(cors())

// Proxy's route
app.get('/cars', (req, res) => hotwire(req.query).then(data => res.send(data)))
app.get('/autocomplete', (req, res) => googleplaces(req.query).then(data => res.send(data)))

// Starts the proxy server
app.listen(3000, () => console.log('Proxy Server ready!'))
