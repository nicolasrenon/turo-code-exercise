import React from 'react'
import { render } from 'react-dom'

import CarSearchFlow from 'components/CarSearchFlow'
import 'styles.css'

render(
  <CarSearchFlow />,
  document.getElementById('root')
)
