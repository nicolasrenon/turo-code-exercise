import React from 'react'
import PropTypes from 'prop-types'

import Spinner from 'components/Spinner'

import 'components/Button/styles.css'

const Button = ({ action, text, loading }) => (
  <button type="button" className="button" onClick={action} disabled={loading}>
    {
      loading
      ? <Spinner />
      : text
    }
  </button>
)

Button.propTypes = {
  action: PropTypes.func.isRequired,
  text: PropTypes.string.isRequired,
  loading: PropTypes.bool.isRequired,
}

export default Button
