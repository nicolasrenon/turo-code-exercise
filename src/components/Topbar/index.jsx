import React from 'react'
import PropTypes from 'prop-types'

import 'components/Topbar/styles.css'

const Topbar = ({ homepage, action }) => (
  <div className="topbar">
    {
      homepage
      ? <img src="logo-turo.png" height="32" alt="Turo" />
      : (
        <button onClick={action}>
          <svg>
            <use xlinkHref="#arrow-left" />
          </svg>
        </button>
      )
    }
  </div>
)

Topbar.propTypes = {
  homepage: PropTypes.bool,
  action: PropTypes.func,
}

Topbar.defaultProps = {
  homepage: false,
  action: () => {},
}

export default Topbar
