import React, { Component } from 'react'
import PropTypes from 'prop-types'
import 'whatwg-fetch'

import PlacesList from 'components/PlacesList'

import 'components/SearchLocation/styles.css'

class SearchLocation extends Component {
  constructor(props) {
    super(props)

    this.state = { places: [] }
    this.debounce = null

    this.handleSelectLocation = this.handleSelectLocation.bind(this)
  }

  handleChange(value) {
    clearTimeout(this.debounce)
    this.debounce = setTimeout(() => {
      fetch(`http://localhost:3000/autocomplete?input=${value}`)
        .then(res => res.json())
        .then(places => this.setState({ places }))
        .catch(error => console.warn('Something went wrong', error))
    }, 400)
  }

  handleSelectLocation(place) {
    this.props.selectLocation(place.textContent)
  }

  render() {
    return (
      <div className="searchLocation">
        <input
          type="text"
          ref={(input) => { this.input = input }}
          placeholder="Enter city, airport or address"
          onChange={e => this.handleChange(e.currentTarget.value)}
        />
        {
          this.state.places.length > 0 &&
          <PlacesList places={this.state.places} selectPlace={this.handleSelectLocation} />
        }
      </div>
    )
  }
}

SearchLocation.propTypes = {
  selectLocation: PropTypes.func.isRequired,
}

export default SearchLocation
