import React from 'react'
import PropTypes from 'prop-types'
import { CSSTransitionGroup } from 'react-transition-group'

import 'components/ActionScreen/styles.css'

const Action = (props) => {
  const childrenArray = React.Children.toArray(props.children)
  return childrenArray[0] || null
}

const ActionScreen = ({ children, condition, enter, leave, enterTimeout, leaveTimeout }) => (
  <CSSTransitionGroup
    component={Action}
    transitionName="actionScreen"
    transitionEnter={enter}
    transitionLeave={leave}
    transitionEnterTimeout={enterTimeout}
    transitionLeaveTimeout={leaveTimeout}
  >
    {
      condition && <div className="actionScreen">{children}</div>
    }
  </CSSTransitionGroup>
)

ActionScreen.defaultProps = {
  enter: true,
  leave: true,
  enterTimeout: 0,
  leaveTimeout: 0,
}

Action.propTypes = {
  children: PropTypes.arrayOf(PropTypes.element).isRequired,
}

ActionScreen.propTypes = {
  children: PropTypes.element.isRequired,
  condition: PropTypes.bool.isRequired,
  enter: PropTypes.bool,
  leave: PropTypes.bool,
  enterTimeout: PropTypes.number,
  leaveTimeout: PropTypes.number,
}

export default ActionScreen
