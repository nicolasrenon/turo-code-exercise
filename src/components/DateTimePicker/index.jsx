import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { SingleDatePicker } from 'react-dates'
import moment from 'moment'

import Button from 'components/Button'
import Topbar from 'components/Topbar'

import 'react-dates/lib/css/_datepicker.css'
import 'components/DateTimePicker/styles.css'

class DateTimePicker extends Component {
  constructor(props) {
    super(props)

    const { date, time } = this.props.datetime
    const dateConverted = date === '' ? moment() : moment(date, 'MM/DD/YYYY')

    this.state = { date: dateConverted, time, focused: false }
    this.submitDate = this.submitDate.bind(this)
  }

  submitDate() {
    const { state: { date }, time } = this
    this.props.handleDate(date.format('MM/DD/YYYY'), time.value)
  }

  render() {
    const { title, buttonText, waitResponse } = this.props

    return (
      <section className="dateTimePicker">
        <Topbar action={this.props.handleBackStep} />
        <h2>{title}</h2>
        <SingleDatePicker
          date={this.state.date}
          onDateChange={date => this.setState({ date })}
          focused={this.state.focused}
          onFocusChange={({ focused }) => this.setState({ focused })}
          numberOfMonths={1}
        />
        <input type="text" defaultValue="10:00" ref={(input) => { this.time = input }} />
        <div className="dateTimePicker_cta">
          <Button action={this.submitDate} text={buttonText} loading={waitResponse} />
        </div>
      </section>
    )
  }
}

DateTimePicker.propTypes = {
  datetime: PropTypes.shape({
    date: PropTypes.string.isRequired,
    time: PropTypes.string.isRequired,
  }).isRequired,
  handleDate: PropTypes.func.isRequired,
  handleBackStep: PropTypes.func,
  title: PropTypes.string.isRequired,
  buttonText: PropTypes.string.isRequired,
  waitResponse: PropTypes.bool,
}

DateTimePicker.defaultProps = {
  handleBackStep: () => {},
  waitResponse: false,
}

export default DateTimePicker
