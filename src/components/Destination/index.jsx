import React from 'react'
import PropTypes from 'prop-types'

import SearchLocation from 'components/SearchLocation'
import Topbar from 'components/Topbar'

import 'components/Destination/styles.css'

const Destination = ({ title, handleLocation }) => (
  <section className="destination">
    <Topbar homepage />
    <h2>{title}</h2>
    <SearchLocation selectLocation={handleLocation} />
  </section>
)

Destination.propTypes = {
  title: PropTypes.string.isRequired,
  handleLocation: PropTypes.func.isRequired,
}

export default Destination
