import React from 'react'

import 'components/Spinner/styles.css'

const Spinner = () => (
  <div className="sk-three-bounce">
    <div className="sk-child sk-bounce1" />
    <div className="sk-child sk-bounce2" />
    <div className="sk-child sk-bounce3" />
  </div>
)

export default Spinner
