import React from 'react'
import PropTypes from 'prop-types'

import 'components/Car/styles.css'

const Car = ({ link, category, type, airport, price }) => (
  <div className="car">
    <h4 className="car_category">{category}</h4>
    <div className="car_details">
      <div className="car_picture">
        <img src="car_sample.jpg" alt={category} />
      </div>
      <div className="car_booking">
        <p>{type}</p>
        <p>
          <svg>
            <use xlinkHref="#airport" />
          </svg>
          {airport}
        </p>
        <p className="car_price">${price}</p>
        <a href={link} className="button button-small">Book</a>
      </div>
    </div>
  </div>
)

Car.propTypes = {
  link: PropTypes.string.isRequired,
  category: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  airport: PropTypes.string.isRequired,
  price: PropTypes.string.isRequired,
}

export default Car
