import React, { Component } from 'react'
import PropTypes from 'prop-types'

class PlacesList extends Component {
  handleClick(item) {
    if (this.list.querySelector('.is-active')) {
      this.list.querySelector('.is-active').classList.remove('is-active')
    }

    item.classList.add('is-active')
    this.props.selectPlace(item)
  }

  render() {
    const items = this.props.places.map(({ id, description }) => (
      <li key={id} onClick={e => this.handleClick(e.currentTarget)}>
        <span>
          {
            description.substr(0, description.length - 15)
            /* Temporary trick to remove ", United States" */
          }
        </span>
      </li>
    ))
    return <ul ref={(list) => { this.list = list }}>{items}</ul>
  }
}

PlacesList.propTypes = {
  places: PropTypes.arrayOf(PropTypes.object).isRequired,
  selectPlace: PropTypes.func.isRequired,
}

export default PlacesList
