import React, { Component } from 'react'
import 'whatwg-fetch'

import ActionScreen from 'components/ActionScreen'
import CarsList from 'components/CarsList'
import DateTimePicker from 'components/DateTimePicker'
import Destination from 'components/Destination'

class CarSearchFlow extends Component {
  constructor(props) {
    super(props)

    this.state = {
      step: 1,
      dest: '',
      startdate: '',
      enddate: '',
      pickuptime: '',
      dropofftime: '',
      types: [],
      cars: [],
      waitingResponse: false,
    }

    this.handleBackStep = this.handleBackStep.bind(this)
    this.handleLocation = this.handleLocation.bind(this)
    this.handleStartDate = this.handleStartDate.bind(this)
    this.handleEndDate = this.handleEndDate.bind(this)
  }

  handleBackStep() {
    this.setState({ step: this.state.step - 1 })
  }

  handleLocation(dest) {
    this.setState({ step: 2, dest })
  }

  handleStartDate(startdate, pickuptime) {
    this.setState({ step: 3, startdate, pickuptime })
  }

  handleEndDate(enddate, dropofftime) {
    this.setState({ waitingResponse: true })
    const { dest, startdate, pickuptime } = this.state
    fetch(`http://localhost:3000/cars?dest=${dest}&startdate=${startdate}&enddate=${enddate}&pickuptime=${pickuptime}&dropofftime=${dropofftime}`)
      .then(res => res.json())
      .then(({ types, cars }) => this.setState({
        step: 4,
        enddate,
        dropofftime,
        types,
        cars,
        waitingResponse: false,
      }))
      .catch(error => console.warn('Something went wrong', error))
  }

  render() {
    const { step, startdate, pickuptime, enddate, dropofftime, waitingResponse } = this.state
    return (
      <div className="carSearch">
        <Destination
          title="Where do you need a car?"
          handleLocation={this.handleLocation}
        />
        <ActionScreen condition={step >= 2} enterTimeout={400} leaveTimeout={400}>
          <DateTimePicker
            title="When would you like to pick-up your car?"
            datetime={{ date: startdate, time: pickuptime }}
            handleBackStep={this.handleBackStep}
            handleDate={this.handleStartDate}
            buttonText="Confirm"
          />
        </ActionScreen>
        <ActionScreen condition={step >= 3} enterTimeout={400} leaveTimeout={400}>
          <DateTimePicker
            title="When would you like to drop the car off?"
            datetime={{ date: enddate, time: dropofftime }}
            handleBackStep={this.handleBackStep}
            handleDate={this.handleEndDate}
            buttonText="Find a car"
            waitResponse={waitingResponse}
          />
        </ActionScreen>
        <ActionScreen condition={step >= 4} enterTimeout={400} leaveTimeout={400}>
          <CarsList
            search={this.state}
            handleBackStep={this.handleBackStep}
          />
        </ActionScreen>
      </div>
    )
  }

}

export default CarSearchFlow
