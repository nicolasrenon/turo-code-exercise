import React from 'react'
import PropTypes from 'prop-types'
import moment from 'moment'

import Car from 'components/Car'
import Topbar from 'components/Topbar'

import 'components/CarsList/styles.css'

const CarsList = ({ search: { dest, startdate, enddate, pickuptime, dropofftime, types = [],
cars = [] }, handleBackStep }) => {
  const list = cars.map(({ HWRefNumber, CarTypeCode, TotalPrice, DeepLink, PickupAirport }) => {
    const typeOfCar = types.find(type => type.CarTypeCode === CarTypeCode)
    return (
      <li key={HWRefNumber}>
        <Car
          link={DeepLink}
          category={typeOfCar.PossibleModels}
          type={typeOfCar.CarTypeName}
          airport={PickupAirport}
          price={TotalPrice}
        />
      </li>
    )
  })

  const title = list.length > 0 ? 'Here are the cars we found for you!' : 'Sorry, we didn\'t find any car :('
  const entryFormat = 'MM/DD/YYYY'
  const outputFormat = 'dddd, MMMM Do YYYY'

  return (
    <section className="carsList">
      <Topbar action={handleBackStep} />
      <h2>{title}</h2>
      <p>
        Near {dest},<br />
        from {moment(startdate, entryFormat).format(outputFormat)} at {pickuptime},<br />
        to {moment(enddate, entryFormat).format(outputFormat)} at {dropofftime}.
      </p>
      {
        list.length > 0 &&
        <ol>{list}</ol>
      }
    </section>
  )
}

CarsList.propTypes = {
  search: PropTypes.shape({
    dest: PropTypes.string.isRequired,
    startdate: PropTypes.string.isRequired,
    enddate: PropTypes.string.isRequired,
    pickuptime: PropTypes.string.isRequired,
    dropofftime: PropTypes.string.isRequired,
    types: PropTypes.array,
    cars: PropTypes.array,
  }).isRequired,
  handleBackStep: PropTypes.func.isRequired,
}

export default CarsList
