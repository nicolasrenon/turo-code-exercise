Turo code exercise
==================

Candidate: Nicolas Renon
Exercise picked: Client (Front-end) / Search / Web

----------

Disclaimer
-------------

This exercise has been done on environment using Node.js v8.1.0 and NPM v5.0.3.
Nevertheless, it should work properly with the previous versions!

Run it
------

After cloning the repository you need to install the dependencies

    npm install

Then you can start the development server

	npm start

And finally start the proxy server to get data from the API

    node apiproxy.js

Now you can open http://localhost:8080 !

The stack
---------

- React
- Webpack
- Babel

Development environment
-----------------------

The dev environment is minimalist but efficient enough to write code in good condition. It uses Webpack as a build system and Babel to transpile modern Javascript syntax (ES2015 & ES2016) to ES5.

I had to use a Node.js server (apiproxy.js) in order to avoid CORS issues.

Which is missing in the project:

- A real CSS compiler (PostCSS, Autoprefixer, ...)
- Unit tests
- CSS & JS linter
- Redux (why not in order to simplify the state management?)
- HMR
- Server side rendering
- ...

UX strategy
===========

The concept is 1 action per screen, I believe it makes the process more interactive and easier on a mobile device.
The Sketch file is available in the repository.

Important notes
===============

1. Not made for large screens! I imagined it as a mobile website, so please use the mobile simulator in Chrome Dev Tools to have a narrow screen :)
2. I tried to respect the Turo's brand guidelines (logo, colors, inputs)
3. For now, I'm not checking the dates From/To, so please make sure you select dates in the future! ;)
4. The inputs of pick-up and drop-off are basic text fields for now, I'll try to make it better soon!
